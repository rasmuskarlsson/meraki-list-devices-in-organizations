import meraki
import os
import dotenv

dotenv.load_dotenv()

# Global variables
apikey = os.getenv("MERAKI_APIKEY")
dashboard = meraki.DashboardAPI(api_key=apikey, suppress_logging=True)
amount_of_orgs = 0
amount_of_orgs_without_api = 0
name_of_orgs_without_api = []
total_mx = 0
total_vmx = 0
total_z3 = 0
total_ms = 0
total_mr = 0
total_mv = 0
total_mt = 0
total_mg = 0

def gather_organizations():
    org_list = dashboard.organizations.getOrganizations()
    return org_list

def amount_of_devices_in_organization(organizationId, organizationName):
    device_list = dashboard.organizations.getOrganizationDevices(organizationId=organizationId, total_pages='all')
    mx = 0
    vmx = 0
    z3 = 0
    ms = 0
    mr = 0
    mv = 0
    mt = 0
    mg = 0
    for device in device_list:
        if device["model"][:2] == "MX":
            mx += 1
        elif device["model"][:3] == "VMX":
            vmx += 1
        elif device["model"][:2] == "Z3":
            z3 += 1
        elif device["model"][:2] == "MS":
            ms += 1
        elif device["model"][:2] == "MR":
            mr += 1
        elif device["model"][:2] == "MV":
            mv += 1
        elif device["model"][:2] == "MT":
            mt += 1
        elif device["model"][:2] == "MG":
            mg += 1
        else:
            print(f"Found device {device['model']} in organization {organizationName}\n")


    return mx, vmx, z3, ms, mr, mv, mt, mg

if __name__ == "__main__":
    org_list = gather_organizations()

    for org in org_list:
        amount_of_orgs += 1
        if org["api"]["enabled"] == True:
            mx, vmx, z3, ms, mr, mv, mt, mg = amount_of_devices_in_organization(organizationId=org["id"], organizationName=org["name"])
            print(f"Organization {org['name']} has:\n{mx} MX\n{vmx} VMX\n{z3} Z3\n{ms} MS\n{mr} MR\n{mv} MV\n{mt} MT\n{mg} MG\n")
            total_mx += mx
            total_vmx += vmx
            total_z3 += z3
            total_ms += ms
            total_mr += mr
            total_mv += mv
            total_mt += mt
            total_mg += mg
        else:
            print(f"API is not active in organization {org['name']}\n")
            name_of_orgs_without_api.append(org["name"])
            amount_of_orgs_without_api += 1

    print(f"Number of orgs checked: {amount_of_orgs}\n")
    print(f"Number of orgs where API is not activated: {amount_of_orgs_without_api}")
    if name_of_orgs_without_api:
        print("Organizations where API is not activated:")
        for org in name_of_orgs_without_api:
            print(org)
    print(f"\nTotal of devices is:\n{total_mx} MX\n{total_vmx} VMX\n{total_z3} Z3\n{total_ms} MS\n{total_mr} MR\n{total_mv} MV\n{total_mt} MT\n{total_mg} MG")
        